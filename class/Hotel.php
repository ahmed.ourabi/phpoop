<?php 
class Hotel{
    public function getHotel(int $id){
        $data = $this->callApi('hotels/'.$id);
        return $data;

    }
    public function getHotels()
    {
        $data = $this->callApi('hotels.json');
        foreach ($data as $hotel){
            //$n++;
            $results[] = [
                'id'=> $hotel['id'],
                'nom'=> $hotel['nom'],
                'categorie'=>$hotel['categorie'],
                'ville'=>$hotel['ville']['nom']
            ];
        }
        return $results;        
    }
    private function callApi(string $endpoint): ?array
    {
        $curl = curl_init('https://213.186.33.40/api/'.$endpoint);
        curl_setopt_array($curl, [
	        CURLOPT_SSL_VERIFYPEER => 0,
	        CURLOPT_RETURNTRANSFER => 1,
	        CURLOPT_TIMEOUT => 2
        ]);
        $data = curl_exec($curl);
        if ( $data === false || curl_getinfo($curl, CURLINFO_HTTP_CODE) !== 200){
            //var_dump(curl_error($curl)); 
            //var_dump(curl_getinfo($curl, CURLINFO_HTTP_CODE));
            return null;
        }
        
        //$n = 0;
        return json_decode($data,true);
    }
}