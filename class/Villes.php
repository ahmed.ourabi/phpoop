<?php 
class Ville{


    public function getVilles()
    {
        $curl = curl_init('https://www.my-freedomtravel.com/api/villes.json');
        curl_setopt_array($curl, [
	        CURLOPT_SSL_VERIFYPEER => 0,
	        CURLOPT_RETURNTRANSFER => 1,
	        CURLOPT_TIMEOUT => 2
        ]);
        //curl_setopt($curl,CURLOPT_SSL_VERIFYPEER, false);
        //curl_setopt($curl,CURLOPT_RETURNTRANSFER, true);
        $data = curl_exec($curl);
        if ( $data === false || curl_getinfo($curl, CURLINFO_HTTP_CODE) !== 200){
            //var_dump(curl_error($curl)); 
            //var_dump(curl_getinfo($curl, CURLINFO_HTTP_CODE));
            return null;
        }
        $results = [];
        $n = 0;
        $data = json_decode($data,true);
        foreach ($data as $ville){
            $n++;
            $results = [
                'id'=> $ville['id'],
                'nom'=> $ville['nom']
            ];
        }
        curl_close($curl);
        return $results;
        //echo '<pre>';
	    //var_dump($data['0']['nom']);
	    //var_dump($data);
	    //echo '</pre>';
        
    }
}