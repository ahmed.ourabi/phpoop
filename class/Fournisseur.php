<?php 
class Fournisseur{
    private $id;
    private $type;
    private $nom;
    private $tel;
    private $fax;
    private $mail;
    private $adresse;
    private $respensable;
    private $contact_resp;
    private $mat_fiscale;
    private $rib;

    public function getId(){
        return $this->id;
    }
    public function getType(){
        return $this->type;
    }
    public function setType($type){
        $this->type = $type;
    }
    
    public function getNom(){
        return $this->nom;
    }
    public function setNom($nom){
        $this->nom = $nom;
    }
    public function getTel(){
        return $this->tel;
    }
    public function setTel($tel){
        $this->tel = $tel;
    }
    public function getFax(){
        return $this->fax;
    }
    public function setFax($fax){
         $this->fax = $fax;
    }
    public function getMail(){
        return $this->mail;
    }
    public function setMail($mail){
        $this->mail = $mail;
    }
    public function getAdresse(){
        return $this->adresse;
    }
    public function setAdresse($adresse){
        $this->adresse = $adresse;
    }
    public function getRespensable(){
        return $this->respensable;
    }
    public function setRespensable($respensable){
        $this->respensable = $respensable;
    }
    public function getContactRespensable(){
        return $this->contact_resp;
    }
    public function setContactRespensable($contact_resp){
        $this->contact_resp = $contact_resp;
    }
    public function getMatFiscale(){
        return $this->mat_fiscale;
    }
    public function setMatFiscale($mat_fiscale){
        $this->mat_fiscale = $mat_fiscale;
    }
    public function getRib(){
        return $this->rib;
    }
    public function setRib($rib){
        $this->rib = $rib;
    }
}