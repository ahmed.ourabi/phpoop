<?php
require_once 'db_connect.php';
$reponse = $bdd->query('SELECT * FROM arrangement');
require_once 'header.php';
?>

    <div class="container">
    <!-- Button trigger modal -->
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
        Ajouter Arrangement
        </button>

        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ajouter Arrangement</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="" method="">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Arrangement</label>
                        <input type="text" class="form-control" name="arrangement" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Arrangement">
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                <button type="submit" class="btn btn-primary">Enregistrer</button>
            </form>
            </div>
            </div>
        </div>
        </div>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Arrangement</th>
            </tr>
        </thead>
        <tbody>
        <?php
            while ($donnees = $reponse->fetch())
            {
                ?>
                <tr>
                    <th scope="row">
                        <?= $donnees['id'] ?>
                    </th>
                    <td>
                        <?= $donnees['nom'] ?>
                    </td>
                </tr>
            
            <?php
            }
            $reponse->closeCursor();
        ?>   
        </tbody>
    </table>
    
    </div>

    
<?php
   require_once 'footer.php';
?>